FROM alpine:3.11.2
RUN apk add --no-cache openjdk11
COPY build/libs/spring-reactive-demo-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/spring-reactive-demo-0.0.1-SNAPSHOT.jar"]