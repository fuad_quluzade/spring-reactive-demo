package az.ingress;

import az.ingress.controller.ProductController;
import az.ingress.dto.ProductDto;
import az.ingress.service.ProductService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.core.AutoConfigureCache;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import  static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebFluxTest(ProductController.class)
class SpringReactiveDemoApplicationTests {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private ProductService service;

    @Test
    public void addProduct(){
        Mono<ProductDto> productDtoMono=Mono.just(new ProductDto("1","mobile",1,1000));

        when(service.saveProduct(productDtoMono)).thenReturn(productDtoMono);

        webTestClient.post().uri("/products")
                .body(Mono.just(productDtoMono),ProductDto.class)
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    public void getAllProducts(){
        Flux<ProductDto> productDtoFlux=Flux.just(new ProductDto("1","mobile",1,1000),
                new ProductDto("2","TV",1,567));

        when(service.getAllProducts()).thenReturn(productDtoFlux);

        Flux<ProductDto> responseBody = webTestClient.get().uri("/products")
                .exchange()
                .expectStatus().isOk()
                .returnResult(ProductDto.class)
                .getResponseBody();

        StepVerifier.create(responseBody)
                .expectSubscription()
                .expectNext(new ProductDto("1","mobile",1,1000))
                .expectNext( new ProductDto("2","TV",1,567))
                .verifyComplete();
    }

    @Test
    public void getProductTest(){
        Mono<ProductDto> productDtoMono=Mono.just(new ProductDto("102","mobile",1,10000));
        when(service.getProduct(any())).thenReturn(productDtoMono);

        Flux<ProductDto> responseBody = webTestClient.get().uri("/products/102")
                .exchange()
                .expectStatus().isOk()
                .returnResult(ProductDto.class)
                .getResponseBody();

        StepVerifier.create(responseBody)
                .expectSubscription()
                .expectNextMatches(p->p.getName().equals("mobile"))
                .verifyComplete();
    }


    @Test
    public void updateProductTest(){
        Mono<ProductDto> productDtoMono=Mono.just(new ProductDto("2","Tv",5,300));
        when(service.updateProduct(productDtoMono,"2")).thenReturn(productDtoMono);

        webTestClient.put().uri("/products/update/2")
                .body(Mono.just(productDtoMono),ProductDto.class)
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    public void deleteProductTest(){
        given(service.deleteProduct(any())).willReturn(Mono.empty());
        webTestClient.delete().uri("/products/delete/102")
                .exchange()
                .expectStatus().isOk();//200
    }


}
