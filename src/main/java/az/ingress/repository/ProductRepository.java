package az.ingress.repository;

import az.ingress.dto.ProductDto;
import az.ingress.entity.Product;
import org.springframework.data.domain.Range;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
//ReactiveCrudRepository<Product,Long>

@Repository
public interface ProductRepository extends  ReactiveMongoRepository<Product,String>{
    Flux<ProductDto> findByPriceBetween(Range<Double> priceRanges);


}


